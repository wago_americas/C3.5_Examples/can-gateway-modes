# Can Gateway Modes

## Project Details
- [ ] Written in Structured Text
- [ ] Codesys Version 3.5.19.30
- [ ] Target: PFC200 750-8212 FW26
- [ ] No Visualizations

## Name
Can Gateway Modes Example Project

## Description
Example of using a 750-658 CAN Gateway module for each of the supported modes (Sniffer, Transparent, and Mapped).

- ![CAN_Sniffer PDF](CAN_Sniffer.pdf)
- ![CAN_Transparent PDF](CAN_Transparent.pdf)
- ![CAN_Mapped PDF](CAN_Mapped.pdf)

![Program](CANGatewayModes1.png){width=300 height=200}


## Support
This program is for demonstration only and does not include support. May contain bugs. Use at your own risk.

## Authors and acknowledgment
Stephen Foster
May 7th, 2024

## License
Copyright (c) 2024, Stephen Foster, WAGO

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE